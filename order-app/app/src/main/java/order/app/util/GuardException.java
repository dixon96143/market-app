package order.app.util;

public class GuardException extends RuntimeException {

    public GuardException(String message) {
        super(message);
    }
}
