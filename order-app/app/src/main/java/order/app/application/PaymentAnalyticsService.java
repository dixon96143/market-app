package order.app.application;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.kstream.Consumed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.support.serializer.JsonSerde;
import org.springframework.stereotype.Service;
import order.app.port.PaymentMessageModel;

import javax.annotation.PostConstruct;

@Service
public class PaymentAnalyticsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentAnalyticsService.class);

    private final StreamsBuilder streamsBuilder;

    public PaymentAnalyticsService(StreamsBuilder streamsBuilder) {
        this.streamsBuilder = streamsBuilder;
    }

    @PostConstruct
    public void analyze() {
        LOGGER.info("Starting kafka stream");
        streamsBuilder.stream("test", Consumed.with(Serdes.String(), jsonSerde()))
                .filter((key, message) -> "DECLINED".equals(message.getPaymentStatus()))
                .foreach((key, value) -> LOGGER.info("Received message {}", value));
    }

    // TODO вытащить в отдельный бин, наверное
    private JsonSerde<PaymentMessageModel> jsonSerde() {
        var jsonSerde = new JsonSerde<>(PaymentMessageModel.class);
        jsonSerde.deserializer().addTrustedPackages("*");
        jsonSerde.deserializer().setRemoveTypeHeaders(false);
        jsonSerde.deserializer().setUseTypeMapperForKey(true);
        return jsonSerde;
    }
}
