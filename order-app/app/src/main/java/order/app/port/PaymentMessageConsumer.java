package order.app.port;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class PaymentMessageConsumer {

    @KafkaListener(topics = "test")
    public PaymentMessageModel paymentMessage(PaymentMessageModel message) {
        return message;
    }
}
