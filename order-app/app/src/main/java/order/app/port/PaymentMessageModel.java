package order.app.port;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PaymentMessageModel {

    private String orderId;

    private String userId;

    private String paymentId;

    private String amount;

    private String paymentStatus;

    @JsonCreator
    public PaymentMessageModel(
            @JsonProperty("orderId") String orderId,
            @JsonProperty("userId") String userId,
            @JsonProperty("paymentId") String paymentId,
            @JsonProperty("amount") String amount,
            @JsonProperty("paymentStatus") String paymentStatus) {
        this.orderId = orderId;
        this.userId = userId;
        this.paymentId = paymentId;
        this.amount = amount;
        this.paymentStatus = paymentStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getUserId() {
        return userId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getAmount() {
        return amount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Override
    public String toString() {
        return "PaymentMessageModel{" +
                "orderId='" + orderId + '\'' +
                ", userId='" + userId + '\'' +
                ", paymentId='" + paymentId + '\'' +
                ", amount='" + amount + '\'' +
                ", paymentStatus='" + paymentStatus + '\'' +
                '}';
    }
}
