CREATE TABLE order_detail (
    order_id serial NOT NULL PRIMARY KEY,
    customer_id bigint NOT NULL,
    creation_date timestamp NOT NULL,
    is_paid boolean NOT NULL,
    total_price decimal NOT NULL
);

CREATE TABLE menu_item (
    menu_item_id serial NOT NULL PRIMARY KEY,
    name varchar(128) NOT NULL,
    price decimal NOT NULL,
    CONSTRAINT price_is_positive CHECK (price > 0)
);

CREATE TABLE order_item (
    order_item serial NOT NULL PRIMARY KEY,
    order_id int NOT NULL ,
    menu_item_id int NOT NULL,
    quantity int NOT NULL,
    CONSTRAINT quantity_is_positive CHECK (quantity > 0),
    CONSTRAINT fk_order
        FOREIGN KEY (order_id)
        REFERENCES order_detail (order_id),
    CONSTRAINT fk_menu_item
        FOREIGN KEY (menu_item_id)
        REFERENCES menu_item (menu_item_id)
);