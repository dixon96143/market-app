import json
import random
import time
import uuid
import requests

application_url = "http://localhost:8080"
payment_path = "/api/payment/receive"

def id():
    return str(uuid.uuid4())[:8]

for i in range(1, 10):
    payment_data = {
        'userId': id(),
        'orderId': id(),
        'amount': random.randrange(100, 1000),
        'paymentId': id()
    }
    requests.post(
        application_url + payment_path,
        data= json.dumps(payment_data),
        headers={'Content-Type': 'application/json'}
    )
    time.sleep(0.5)
