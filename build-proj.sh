#!/bin/bash

cd ./delivery-app/docker
bash ./build-image.sh

cd ../../order-app/docker
bash ./build-image.sh

cd ../../payment-processing-app/docker
bash ./build-image.sh