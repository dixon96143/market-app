package test.kafka.application.payment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import test.kafka.application.messaging.PaymentMessageProvider;
import test.kafka.domain.order.OrderId;
import test.kafka.domain.payment.*;
import test.kafka.domain.user.UserId;
import test.kafka.port.model.PaymentMessageModel;
import test.kafka.port.model.PaymentRequestBody;

@Service
public class PaymentProcessingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentProcessingService.class);

    private final PaymentMessageProvider paymentMessageProvider;

    public PaymentProcessingService(PaymentMessageProvider paymentMessageProvider) {
        this.paymentMessageProvider = paymentMessageProvider;
    }

    public void processPayment(PaymentRequestBody paymentRequestBody) {
        var unprocessedPayment = from(paymentRequestBody);
        var processedPayment = processPayment(unprocessedPayment);
        paymentMessageProvider.sendPaymentMessageToKafka(fromProcessedPayment(processedPayment));
        LOGGER.info("Payment: {}", processedPayment);
    }

    //Рандомно "обрабатываем" оплату. Сделаем так, чтобы примено 30% оплат не проходило
    private ProcessedPayment processPayment(Payment payment) {
        if (Math.random() < 0.3) {
            return new ProcessedPayment(payment, PaymentStatus.DECLINED);
        }
        else {
            return new ProcessedPayment(payment, PaymentStatus.SUCCEEDED);
        }
    }

    private static PaymentMessageModel fromProcessedPayment(ProcessedPayment processedPayment) {
        return new PaymentMessageModel(
               processedPayment.getPayment().getOrderId().getOrderId(),
               processedPayment.getPayment().getUserId().getUserId(),
               processedPayment.getPayment().getPaymentId().getPaymentId(),
               String.valueOf(processedPayment.getPayment().getPaymentAmount().getAmount()),
               processedPayment.getPaymentStatus().name()
        );
    }

    //TODO - тут может быть ошибка парсинга long для payment amount. Надо превратить в 400 Bad request
    private static Payment from(PaymentRequestBody paymentRequestBody) {
        return new Payment(
                new PaymentId(paymentRequestBody.getPaymentId()),
                new PaymentAmount(Long.parseLong(paymentRequestBody.getAmount())),
                new OrderId(paymentRequestBody.getOrderId()),
                new UserId(paymentRequestBody.getUserId())
        );
    }
}
