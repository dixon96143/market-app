package test.kafka.application.messaging;

import org.apache.kafka.clients.admin.NewTopic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.kafka.KafkaException;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutionException;

@Service
public class KafkaTemporaryService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaTemporaryService.class);

    private final KafkaAdmin kafkaAdmin;

    public KafkaTemporaryService(KafkaAdmin kafkaAdmin) {
        this.kafkaAdmin = kafkaAdmin;
    }


    @EventListener
    protected void init(ApplicationReadyEvent event) {
        kafkaAdmin.getConfigurationProperties();
        try {
            var topics = kafkaAdmin.describeTopics("test");
            topics = kafkaAdmin.describeTopics("test");
            LOGGER.info("Kafka properties : {}", kafkaAdmin.getConfigurationProperties());
            LOGGER.info("Kafka has test topic : {}", !topics.isEmpty());
        }
        catch (KafkaException e)
        {
            kafkaAdmin.createOrModifyTopics(new NewTopic("test", 1, (short) 1));
        }

    }
}
