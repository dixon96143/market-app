package test.kafka.application.messaging;

import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import test.kafka.domain.payment.ProcessedPayment;
import test.kafka.port.model.PaymentMessageModel;

@Service
public class PaymentMessageProvider {

    private final KafkaTemplate<String, PaymentMessageModel> kafkaTemplate;

    public PaymentMessageProvider(KafkaTemplate<String, PaymentMessageModel> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
    }

    public void sendPaymentMessageToKafka(PaymentMessageModel processedPayment) {
        kafkaTemplate.send("test", processedPayment);
    }
}
