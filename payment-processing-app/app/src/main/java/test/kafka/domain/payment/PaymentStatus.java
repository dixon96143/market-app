package test.kafka.domain.payment;

public enum PaymentStatus {
    SUCCEEDED,
    DECLINED
}
