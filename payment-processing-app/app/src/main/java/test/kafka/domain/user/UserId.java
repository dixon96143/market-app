package test.kafka.domain.user;

import org.springframework.lang.NonNull;

import static test.kafka.utils.Guard.argNotNull;

public class UserId {

    @NonNull
    private final String userId;

    public UserId(@NonNull String userId) {
        this.userId = argNotNull(userId, "userId must be specified");
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "UserId{" +
                "userId='" + userId + '\'' +
                '}';
    }
}
