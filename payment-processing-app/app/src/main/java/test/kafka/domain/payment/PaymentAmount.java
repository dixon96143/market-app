package test.kafka.domain.payment;

public class PaymentAmount {

    // не буду сильно выдумывать, буду использовать целые числа
    private final long amount;

    public PaymentAmount(long amount) {
        this.amount = amount;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "PaymentAmount{" +
                "amount=" + amount +
                '}';
    }
}
