package test.kafka.domain.order;

import org.springframework.lang.NonNull;

import static test.kafka.utils.Guard.argNotBlank;

public class OrderId {

    @NonNull
    private final String orderId;

    public OrderId(@NonNull String orderId) {
        this.orderId = argNotBlank(orderId, "orderId must be specified");
    }

    @NonNull
    public String getOrderId() {
        return orderId;
    }

    @Override
    public String toString() {
        return "OrderId{" +
                "orderId='" + orderId + '\'' +
                '}';
    }
}
