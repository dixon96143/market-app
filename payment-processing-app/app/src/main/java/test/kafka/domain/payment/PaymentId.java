package test.kafka.domain.payment;

import org.springframework.lang.NonNull;

import static test.kafka.utils.Guard.argNotBlank;

public class PaymentId {

    @NonNull
    private final String paymentId;

    public PaymentId(@NonNull String paymentId) {
        this.paymentId = argNotBlank(paymentId, "paymentId must be specified");
    }

    @NonNull
    public String getPaymentId() {
        return paymentId;
    }

    @Override
    public String toString() {
        return "PaymentId{" +
                "paymentId='" + paymentId + '\'' +
                '}';
    }
}
