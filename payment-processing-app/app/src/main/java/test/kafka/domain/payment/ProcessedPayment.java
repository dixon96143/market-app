package test.kafka.domain.payment;

import org.springframework.lang.NonNull;
import test.kafka.utils.Guard;

public class ProcessedPayment {

    @NonNull
    private final Payment payment;

    @NonNull
    private final PaymentStatus paymentStatus;

    public ProcessedPayment(@NonNull Payment payment, @NonNull PaymentStatus paymentStatus) {
        this.payment = Guard.argNotNull(payment, "payment must be specified");
        this.paymentStatus = Guard.argNotNull(paymentStatus, "paymentStatus must be specified");
    }

    @NonNull
    public Payment getPayment() {
        return payment;
    }

    @NonNull
    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    @Override
    public String toString() {
        return "ProcessedPayment{" +
                "payment=" + payment +
                ", paymentStatus=" + paymentStatus +
                '}';
    }
}
