package test.kafka.domain.payment;

import org.springframework.lang.NonNull;
import test.kafka.domain.order.OrderId;
import test.kafka.domain.user.UserId;
import test.kafka.utils.Guard;

public class Payment {

    @NonNull
    private final PaymentId paymentId;

    @NonNull
    private final PaymentAmount paymentAmount;

    @NonNull
    private final OrderId orderId;

    @NonNull
    private final UserId userId;

    public Payment(
            @NonNull PaymentId paymentId,
            @NonNull PaymentAmount paymentAmount,
            @NonNull OrderId orderId,
            @NonNull UserId userId) {
        this.paymentId = Guard.argNotNull(paymentId, "paymentId must be specified");
        this.paymentAmount = Guard.argNotNull(paymentAmount, "paymentAmount must be specified");
        this.orderId = Guard.argNotNull(orderId, "orderId must be specified");
        this.userId = Guard.argNotNull(userId, "userId must be specified");
    }

    @NonNull
    public PaymentId getPaymentId() {
        return paymentId;
    }

    @NonNull
    public PaymentAmount getPaymentAmount() {
        return paymentAmount;
    }

    @NonNull
    public OrderId getOrderId() {
        return orderId;
    }

    @NonNull
    public UserId getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "UnprocessedPayment{" +
                "paymentId=" + paymentId +
                ", paymentAmount=" + paymentAmount +
                ", orderId=" + orderId +
                ", userId=" + userId +
                '}';
    }
}
