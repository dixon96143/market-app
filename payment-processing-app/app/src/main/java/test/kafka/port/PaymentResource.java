package test.kafka.port;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import test.kafka.application.payment.PaymentProcessingService;
import test.kafka.port.model.PaymentRequestBody;

import java.util.Map;

@RestController
public class PaymentResource {

    private final PaymentProcessingService paymentProcessingService;

    public PaymentResource(PaymentProcessingService paymentProcessingService) {
        this.paymentProcessingService = paymentProcessingService;
    }

    // TODO - добавить варианты, когда что-то сломалось, чтоб не 500, а отдавало 400 ошибку
    @PostMapping("/api/payment/receive")
    public ResponseEntity<Map<String, String>> receivePayment(@RequestBody PaymentRequestBody paymentRequestBody) {
        this.paymentProcessingService.processPayment(paymentRequestBody);
        return ResponseEntity.ok(Map.of("info", "Payment has been processed."));
    }
}
