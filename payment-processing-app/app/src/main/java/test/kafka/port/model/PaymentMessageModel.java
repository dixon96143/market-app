package test.kafka.port.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PaymentMessageModel {

    private final String orderId;

    private final String userId;

    private final String paymentId;

    private final String amount;

    private final String paymentStatus;

    public PaymentMessageModel(
            String orderId,
            String userId,
            String paymentId,
            String amount,
            String paymentStatus) {
        this.orderId = orderId;
        this.userId = userId;
        this.paymentId = paymentId;
        this.amount = amount;
        this.paymentStatus = paymentStatus;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getUserId() {
        return userId;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public String getAmount() {
        return amount;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }
}
