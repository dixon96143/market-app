package test.kafka.port.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class PaymentRequestBody {

    private String userId;

    private String orderId;

    private String amount;

    private String paymentId;

    @JsonCreator
    public PaymentRequestBody(
            @JsonProperty("userId") String userId,
            @JsonProperty("orderId") String orderId,
            @JsonProperty("amount") String amount,
            @JsonProperty("paymentId") String paymentId) {
        this.userId = userId;
        this.orderId = orderId;
        this.amount = amount;
        this.paymentId = paymentId;
    }

    public String getUserId() {
        return userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getAmount() {
        return amount;
    }

    public String getPaymentId() {
        return paymentId;
    }
}
