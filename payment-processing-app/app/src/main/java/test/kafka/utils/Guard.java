package test.kafka.utils;

public class Guard<T> {

    private Guard() {
        // nop
    }

    public static <T> T argNotNull(T obj, String message) {
        if (obj == null) {
            throw new GuardException(message);
        }
        else {
            return obj;
        }
    }

    public static <T extends CharSequence> T argNotBlank(T seq, String message) {
        if (message == null || message.length() == 0) {
            throw new GuardException(message);
        }
        else {
            return seq;
        }
    }
}
