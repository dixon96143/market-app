package test.kafka.utils;

public class GuardException extends RuntimeException {

    public GuardException(String message) {
        super(message);
    }
}
